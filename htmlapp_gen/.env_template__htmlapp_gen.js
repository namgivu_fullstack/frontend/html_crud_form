module.exports = {
    page_title: 'your_FORM_TITLE',

    form_field_list: [
        {'name': 'id'},                   // number
        {'name': 'name'},                 // text
        {'name': 'createdAt'},            // datetime
        {'name': 'some_dropdown_field'},  // text
    ],
    field_as_id: 'id',

    RESTFUL_API_URL: 'https://63df5c6959bccf35dab22dc5.mockapi.io/htmlapp_gen',
}
