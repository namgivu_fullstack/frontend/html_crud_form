const fs  = require('fs')
const fse = require('fs-extra')

// load input
let SH    = __dirname
let env_f = `${SH}/.env.js`; if (! fs.existsSync(env_f) ) {throw Error(`Envfile is required at ${env_f}`) }
let env   = require('./.env')
    //region prepare env.xx
                                                                                env.fieldname_list    = env.form_field_list.map(i => {return i.name})
    let l={} ; env.form_field_list.forEach(i => {l[i.name] = i.displayed_as}) ; env.displayed_as_list = l  // map(list) to ojb ref. https://stackoverflow.com/a/14810722/248616

    env.fieldname_list__jsonstr    = JSON.stringify(env.fieldname_list)
    env.displayed_as_list__jsonstr = JSON.stringify(env.displayed_as_list)
    //endregion prepare env.xx

// clone template to output
let TEMPLATE_D = `${SH}/template/`;                           let T=TEMPLATE_D;    if (! fs.existsSync(T) ) { throw Error(`Template folder must exist at ${T}`) }
let OUTPUT_D   = `${SH}/output__code_generated/html_webapp/`; let O=OUTPUT_D;      fs.mkdirSync(O, {recursive: true})
fs.copyFileSync(`${T}/index.html`, `${O}/index.html`)
fse.copySync   (`${T}/js/`,        `${O}/js/`)

let upsertform_html = ''
console.log(`--- Generating upsertform @ ${env.fieldname_list} ...`)
    //region upsert form
    for (let f of env.form_field_list) {
        //TODO fornow we render all fields as :text -> need field's datatype and render accordingly
        let f_html = `
            <div class="form-group mb-3">
                <label  for="${f.name}" class="small" style="color:grey"       >${f.name}</label>
                <input   id="${f.name}"                                   name="${f.name}"    class="form-control" type="text"
                        aria-describedby="helpId__${f.name}" placeholder="Enter ${f.name}" >
                <small                id="helpId__${f.name}" class="form-text text-muted d-none" >e.g. TODO display for datetime value sample here if type=datetime</small>
            </div>
        `
        upsertform_html = `${upsertform_html}\n${f_html}`
    }
    //endregion upsert form

    //NOTE data for listingtable will be loaded fr api :axios request

    //region fill var val to the template
    let template_filepath=`${O}/index.html`
    let vars = {env, upsertform_html}

    const nunjucks = require('nunjucks')
    let rendered_str = nunjucks.render(template_filepath, vars)

    fs.writeFileSync(template_filepath, rendered_str, 'utf8')
    //endregion fill var val to the template

console.log(`--- Generating upsertform @ ${env.fieldname_list} ... Done`)
