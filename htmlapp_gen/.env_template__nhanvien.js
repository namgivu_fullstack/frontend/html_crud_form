module.exports = {
    page_title: 'your_FORM_TITLE',

    form_field_list: [
        {name: 'id',                 displayed_as: 'id'},
        {name: 'maNhanVien',         displayed_as: 'Mã nhân viên'},
        {name: 'tenNhanVien',        displayed_as: 'Tên nhân viên'},
        {name: 'chucVu',             displayed_as: 'Chức vụ'},
        {name: 'luongCoBan',         displayed_as: 'Lương cơ bản'},
        {name: 'soGioLamTrongThang', displayed_as: 'Giờ làm / tháng'},
    ],
    field_as_id: 'id',

    RESTFUL_API_URL: 'https://63df5c6959bccf35dab22dc5.mockapi.io/nhanvien__bainop_axios_fe',
}
