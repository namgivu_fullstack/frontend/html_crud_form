function render_listingtable(d, field_as_id, fieldname_list__jsonstr, displayed_as_list__jsonstr) {
    // parse JSON.stringify() params to js keyval list
    let fieldname_list    = JSON.parse(   fieldname_list__jsonstr)
    let displayed_as_list = JSON.parse(displayed_as_list__jsonstr)

    //region gen :html
    let html = ''

    if (! d) {
        html = 'Empty data'
        return html

    } else {
        let r_list          = d  // just a clearer varname
        let r_list_filtered = []

        //region filter :d with fields in :fieldname_list, keep field listing-order untouched
        for (let r of r_list) {
            let r_filtered = Object()
            for (let fn of fieldname_list) {
                r_filtered[fn] = r[fn]
            }
            r_list_filtered.push(r_filtered)
        }
        r_list = r_list_filtered
        //endregion filter :d with fields in :fieldname_list, keep field listing-order untouched

        //region listingtable col header
        let th_html = Object.keys(r_list[0]).map(fieldname =>
            `<th data-fieldname="${fieldname}">${ displayed_as_list[fieldname] }</th>`
        ).join('\n')
        //endregion listingtable col header

        //region listingtable rows
        let get_td_html = r => Object.keys(r).map(fieldname => {
            if (fieldname === field_as_id) {  // is :id field
                return `<td data-fieldname="${field_as_id}" data-fieldval="${r[field_as_id]}"> 
                            <!--                href added; otherwise, text wont be formatted as link -->
                            <a   class="id_col" href data-toggle="modal" data-target="#upsertform_popup_modalid">
                            <!--                     data-toggle=        data-target=  to trigger popup modal -->
                                ${ r[field_as_id] }
                             </a>
                        </td>`  // id value linked to open :upsertform/:id
            } else {
                return `<td class="fieldval_col" data-fieldname="${fieldname}" data-fieldval="${r[fieldname]}">
                            ${ r[fieldname] }
                        </td>`
            }
        }).join('\n')

        //region handle .col_id clicked --> show upsertform w/ value filled in
        $('body').on('click', 'a.id_col', function() {
        //                                function() must be this; and cant be arrow func here () =>

            //region fill :id
            let id_fieldval  = $(this).closest('td').attr('data-fieldval')
            let id_fieldname = $(this).closest('td').attr('data-fieldname')
            $(`.upsertform input[name='${id_fieldname}']`).val(id_fieldval)
            $(`.upsertform input[name='${id_fieldname}']`).attr('disabled', true)
            //endregion fill :id

            // fill :td
            $(this)
                .closest('tr')
                .find('td.fieldval_col').each( (i,td) => {
                    // fill value
                    let fieldval  = $(td).attr('data-fieldval')
                    let fieldname = $(td).attr('data-fieldname')
                    $(`.upsertform [name='${fieldname}']`).val(fieldval)
                })
        })
        //endregion handle .col_id clicked --> show upsertform w/ value filled in

        let tr_html = r_list.map(r =>
            `<tr id="row_${r[field_as_id]}">
                ${ get_td_html(r) }
            </tr>`
        ).join('\n')
        //endregion listingtable rows

        //region get_demo_row
        function get_demo_row() {
            let listingtable_rowlist_html = ''
            let demo_rownum = 6
            for (let ri=0; ri<demo_rownum; ri++) {
                let tr_html = `
                    <tr id="row${ri}">
                        ${form_fieldname_list.map(
                    (o,i) => `<td>TODO ${o.name.split('_').map(i=>i[0]).join('')}__r${ri}c${i}</td>`
                ).join('\n')}
                    </tr>
                `
                listingtable_rowlist_html = `${listingtable_rowlist_html}\n${tr_html}`
            }
            return listingtable_rowlist_html
        }

        /* turn this on wh you dont have api url to call to */
        // let listingtable_rowlist_html = get_demo_row()
        //endregion get_demo_row

        html = `
        <table class="table table-striped table-bordered">
            <!-- <thead class="thead-dark"> -->
            <thead>
                ${th_html}
            </thead>

            <tbody>
                ${tr_html}
            </tbody>
        </table>
        `
        return html
    }
    //endregion gen :html
}
