function CallAPI(endpoint_url) {

    this.GET_list = ()            => { return axios({ method:'GET',     url:endpoint_url,                          }) }
    this.POST     = (reqbody)     => { return axios({ method:'POST',    url:endpoint_url,            data: reqbody }) }
    this.UPDATE   = (reqbody, id) => { return axios({ method:'PUT',     url:`${endpoint_url}/${id}`, data: reqbody }) }
    this.DELETE   = (         id) => { return axios({ method:'DELETE',  url:`${endpoint_url}/${id}`,               }) }

    //TODO show :loading_overlay until req done/failed
}
